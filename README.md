# RockSweeper

1.0 Installation

RockSweeper is a html5 game which is now an open source game, click on the Download folder on this site, find the RockSweeper.zip file and download it, after you have extracted the content of the zip file, go inside the RockSweeper folder then click on the index.html file and open it with any web browser that you wish to. 

1.1 Play Game

You need to move both the ships to intercept the incoming rock and destroy it. The front ship will cause damage to the rock and the backship will destroy it. Use both the left and right arrow key to move the ship to intercept the incoming rock. Do not intercept the incoming rock with the back ship before the front ship has done so or else it will be game over.

1.2 The source code

You are free to edit the source code of this game and use this game in all manner as you wish to. The assets of this game are not reusable in another project.  

1.3 Ask question

If you have any question regarding this game do let me know by emailing me.